// load .env variables
require('dotenv').load();

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');

const apiV1 = require('./api/v1/routes');

// create server
const server = express();

// server config
server.use(cors());
server.use(morgan('dev'));
server.use(bodyParser.json({
    extended: false,
}));

server.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
});

server.use('/apiV1', apiV1);

server.get('*', (req, res) => {
    res.send('Such page not exist :(');
});

server.listen(process.env.SERVER_PORT, () => {
    console.log(`Server available at http://localhost:${process.env.SERVER_PORT}`)
});

// database setup
mongoose.Promise = global.Promise;

mongoose.connect(process.env.DB_URL, {useNewUrlParser: true})
    .then(() => {
        // console.clear();
        // console.log("Database connection has been established")
    })
    .catch(err => {
        console.log(err)
    });