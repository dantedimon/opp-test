const express = require('express');
const router = express.Router();

const TransactionController = require('../controllers/TransactionController');

router.post('/transaction', TransactionController.handleRequest);

module.exports = router;