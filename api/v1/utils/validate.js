// constance's
const CURRENCY_LIST = [
    'EUR', 'USD'
];

const PAYMENT_BRAND_LIST = [
    'VISA', 'MASTER'
];

const REQUIRED_FIELDS = [
    'amount', 'currency', 'paymentBrand',
    'cardNumber', 'cardHolder', 'cardExpiryMonth',
    'cardExpiryYear', 'cardCVV'
];

// reg expressions
const REGEXP_NUMBERS_ONLY = /^[0-9]*$/gm;
const REGEXP_ALPHABETS_ONLY = /^[a-zA-Z\s]+$/gm;
const REGEXP_AMOUNT = /^[0-9]+(\.[0-9]{1,2})?$/gm;

// helpers
const isString = (v) => {
    return typeof v === 'string'
};

const isNumber = (v) => {
    return typeof v === 'number'
};

const isInt = (v) => {
    return v % 1 === 0;
};

const isFloat = (v) => {
    return v % 1 !== 0;
};

const hasRequiredFields = (data) => {
    let missedFields = [];

    REQUIRED_FIELDS.forEach(field => {

        if (!data.hasOwnProperty(field)) {
            missedFields.push(field);
        }
    });


    const isValid = !missedFields.length;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        let errors = missedFields.map(field => {
            return {
                field: field,
                message: 'This field is required'
            }
        });

        return {
            valid: false,
            errors: errors
        }
    }
};

const validateAmount = (v) => {
    const isValid = new RegExp(REGEXP_AMOUNT).test(v) && Number(v) > 0;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'amount',
                value: v,
                message: 'Amount value is not valid'
            }
        }
    }
};

const validateCurrency = (v) => {
    const isValid = CURRENCY_LIST.indexOf(v) > -1;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'currency',
                value: v,
                message: 'Currency value is not valid'
            }
        }
    }
};

const validatePaymentBrand = (v) => {
    v = v.toString().toUpperCase();

    const isValid = PAYMENT_BRAND_LIST.indexOf(v) > -1;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'paymentBrand',
                value: v,
                message: 'Payment brand value is not valid'
            }
        }
    }
};

const validateCardNumber = (v) => {
    const isOnlyNumbers = new RegExp(REGEXP_NUMBERS_ONLY).test(v);
    const validLength = v.toString().length === 16;

    const isValid = isOnlyNumbers && validLength;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'cardNumber',
                value: v,
                message: 'Card number value is not valid'
            }
        }
    }
};

const validateCardHolder = (v) => {
    const isOnlyAlphabets = new RegExp(REGEXP_ALPHABETS_ONLY).test(v);

    const isValid = isOnlyAlphabets;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'cardHolder',
                value: v,
                message: 'Card holder value is not valid'
            }
        }
    }
};

const validateCardExpiryMonth = (v) => {
    v = Number(v);

    const isOnlyNumbers = new RegExp(REGEXP_NUMBERS_ONLY).test(v);
    const inRightRange = v < 13 && v > 0;

    const isValid = isOnlyNumbers && inRightRange;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'cardExpiryMonth',
                value: v,
                message: 'Card expiry month value is not valid'
            }
        }
    }
};

const validateCardExpiryYear = (v) => {
    v = Number(v);

    const isOnlyNumbers = new RegExp(REGEXP_NUMBERS_ONLY).test(v);
    const inRightRange = v < 2051 && v > 1949;


    const isValid = isOnlyNumbers && inRightRange;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'cardExpiryYear',
                value: v,
                message: 'Card expiry year value is not valid'
            }
        }
    }
};

const validateCardCVV = (v) => {
    v = Number(v);

    const isOnlyNumbers = new RegExp(REGEXP_NUMBERS_ONLY).test(v);
    const validLength = v.toString().length === 3;

    const isValid = isOnlyNumbers && validLength;

    if (isValid) {
        return {
            valid: true,
            error: null
        }
    } else {
        return {
            valid: false,
            error: {
                field: 'cardCVV',
                value: v,
                message: 'Card cvv value is not valid'
            }
        }
    }
};

// main
const validate = (data) => {
    let errors = [];

    let checkFields = hasRequiredFields(data);

    if (checkFields.valid) {
        let validators = {
            amount: validateAmount,
            currency: validateCurrency,
            paymentBrand: validatePaymentBrand,
            cardNumber: validateCardNumber,
            cardHolder: validateCardHolder,
            cardExpiryMonth: validateCardExpiryMonth,
            cardExpiryYear: validateCardExpiryYear,
            cardCVV: validateCardCVV
        };

        for (let field in validators) {
            let result = validators[field](data[field]);

            if (!result.valid) {
                errors.push(result.error)
            }
        }

        if (errors.length) {
            return {
                valid: false,
                errors: errors
            }
        } else {
            return {
                valid: true,
                errors: null
            }
        }
    } else {
        errors = checkFields.errors;

        return {
            valid: false,
            errors: errors
        }
    }
};

module.exports = validate;

// test
let data = {
    amount: '20.75',
    currency: 'EUR',
    paymentBrand: 'VISA',
    cardNumber: '4200000000000000',
    cardHolder: 'dante',
    cardExpiryMonth: '02',
    cardExpiryYear: '2050',
    cardCVV: 123
};

console.log(validate(data));

