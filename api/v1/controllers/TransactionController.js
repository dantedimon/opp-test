const querystring = require('querystring');
const axios = require('axios');
const TransactionModel = require('../models/TransactionModel');

// utils
const validate = require('../utils/validate');

const callToApi = (data, callback) => {
    const path = 'https://test.oppwa.com:443/v1/payments';

    const reqData = querystring.stringify({
        'authentication.userId': '8a8294174b7ecb28014b9699220015cc',
        'authentication.password': 'sy6KJsT8',
        'authentication.entityId': '8a8294174b7ecb28014b9699220015ca',
        'paymentType': 'DB',
        'amount': data.amount,
        'currency': data.currency,
        'paymentBrand': data.paymentBrand,
        'card.number': data.cardNumber,
        'card.holder': data.cardHolder,
        'card.expiryMonth': data.cardExpiryMonth,
        'card.expiryYear': data.cardExpiryYear,
        'card.cvv': data.cardCVV
    });

    axios.post(path, reqData)
        .then(response => {
            callback(response.data)
        })
        .catch(err => {
            console.log(err);
        })
};

// main
const handleRequest = (req, res) => {
    let data = req.body;

    const check = validate(data);

    if (check.valid) {
        callToApi(data, async (jsonRes) => {
            let transaction = new TransactionModel({
                ...jsonRes
            });

            await transaction.save();

            res.json({
                ok: true,
                response: transaction
            })
        });
    } else {
        res.status(400).json({
            ok: false,
            errors: check.errors
        })
    }
};

module.exports = {handleRequest};