const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let TransactionSchema = new Schema({
    transactionID: String,
    amount: String,
    currency: String,
    buildNumber: String,
    descriptor: String,
    ndc: String,
    paymentType: String,
    resultDetails: Object,
    card: Object,
    risk: Object,
    result: Object,
    timestamp: String
});

module.exports = mongoose.model('transaction', TransactionSchema);